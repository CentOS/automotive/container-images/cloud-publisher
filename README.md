# AutoSD Cloud Publisher Image

This contains all the necessary tools to publish existing AutoSD images to cloud providers.

## Building

```
podman build -t localhost/cloud-publisher:latest .
```

## Running

The image contains a script `autosd-cloud-image-pulisher` which is responsible for
both pulling some images from AutoSD's nightly repository and pushing them to cloud providers.

```
podman run -it localhost/cloud-publisher:latest
```

An ansible playbook is provided in `deploy/ansible` to run a container using this image
as a Kubernetes job.

## License

[MIT](./LICENSE)
